package tacos.service;

import tacos.model.RegistrationForm;
import tacos.model.User;

public interface UserDetailsService extends org.springframework.security.core.userdetails.UserDetailsService {
    User save(RegistrationForm form);
}
