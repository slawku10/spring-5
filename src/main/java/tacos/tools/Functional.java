package tacos.tools;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Functional {

    public static <K, T> Map<K, List<T>> getMapGroupingObject(Iterable<T> iterable, Function<? super T, ? extends K> groupingStrategy) {
        return StreamSupport
                .stream(iterable.spliterator(), false)
                .collect(Collectors.groupingBy(groupingStrategy));
    }
}
