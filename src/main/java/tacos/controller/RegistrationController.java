package tacos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import tacos.model.RegistrationForm;
import tacos.service.UserDetailsService;

@Controller
@RequestMapping("/register")
public class RegistrationController {

    private final UserDetailsService userDetailsService;

    @Autowired
    public RegistrationController(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @GetMapping
    public String registerForm(){
        return "registration";
    }

    @PostMapping
    public String processRegistration(RegistrationForm form) {
        userDetailsService.save(form);
        return "redirect:/login";
    }
}
