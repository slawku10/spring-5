package tacos.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import tacos.repository.IngredientRepository;
import tacos.repository.TacoRepository;
import tacos.model.Ingredient;
import tacos.model.Order;
import tacos.model.Taco;

import javax.validation.Valid;

import static tacos.tools.Functional.getMapGroupingObject;

@Slf4j
@Controller
@RequestMapping("/design")
@SessionAttributes("order")
public class DesignTacoController {

    private IngredientRepository ingredientRepository;
    private TacoRepository tacoRepository;

    @Autowired
    public DesignTacoController(IngredientRepository ingredientRepository,
                                TacoRepository tacoRepository) {
        this.ingredientRepository = ingredientRepository;
        this.tacoRepository = tacoRepository;
    }

    @ModelAttribute(name="order")
    public Order order(){
        return new Order();
    }

    @GetMapping
    public String showDesignForm(Model model) {
        model.addAttribute("taco", new Taco());
        model.addAllAttributes(getMapGroupingObject(ingredientRepository.findAll(),
                Ingredient::getTypeAsStringLowerCase));
        return "design";
    }

    @PostMapping
    public String processDesign(@Valid Taco taco, @ModelAttribute Order order, Model model, Errors errors) {
        if (errors.hasErrors()) {
            model.addAllAttributes(getMapGroupingObject(ingredientRepository.findAll(),
                    Ingredient::getTypeAsStringLowerCase));
            return "design";
        }
        order.addDesign(tacoRepository.save(taco));
        log.info("Przetwarzanie projectu taco: " + taco);
        return "redirect:/orders/current";
    }
}
