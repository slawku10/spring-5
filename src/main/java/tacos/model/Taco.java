package tacos.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class Taco {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Date createdOn;
    @NotNull
    @Size(min=5, message="Nazwa musi składać się przynajmniej z 5 znaków")
    private String name;
    @NotNull(message = "Musisz wybrać conajmniej 1 składnik")
    @Size(min=2, message = "Musisz wybrać conajmniej 1 składnik")
    @ManyToMany(targetEntity = Ingredient.class)
    private List<Ingredient> ingredients;

    @PrePersist
    void createAt(){
        createdOn = new Date(System.currentTimeMillis());
    }
}