package tacos.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "USER_AUTHORITIES")
public class UserAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private final Long id;
    private final String username;
    private final String authority;
}
