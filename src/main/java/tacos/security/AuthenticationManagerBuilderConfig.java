package tacos.security;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

abstract class AuthenticationManagerBuilderConfig {
    abstract void configure(AuthenticationManagerBuilder auth) throws Exception;
}
