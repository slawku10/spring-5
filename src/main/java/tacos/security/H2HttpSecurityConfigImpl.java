package tacos.security;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.stereotype.Component;

@Component
@Qualifier("H2HttpSecurityConfigDevImpl")
public class H2HttpSecurityConfigImpl extends HttpSecurityConfig {
    @Override
    void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/design", "/orders")
                .hasRole("USER")
                .antMatchers("/h2-console/**").permitAll()
//                .antMatchers("/h2-console/**").hasRole("ADMIN")
                .antMatchers("/register").permitAll()
                .antMatchers("/", "/**").permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .defaultSuccessUrl("/design")
                .and()
                .logout()
                .logoutSuccessUrl("/");

        http.csrf().disable();
        http.headers().frameOptions().disable();
    }
}
