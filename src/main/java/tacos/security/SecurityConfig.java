package tacos.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final AuthenticationManagerBuilderConfig authenticationConfig;
    private final HttpSecurityConfig httpSecurityConfig;

    @Autowired
    public SecurityConfig(@Qualifier("JpaAuthentication") AuthenticationManagerBuilderConfig authenticationConfig,
                          @Qualifier("H2HttpSecurityConfigDevImpl") HttpSecurityConfig httpSecurityConfig) {
        this.authenticationConfig = authenticationConfig;
        this.httpSecurityConfig = httpSecurityConfig;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        httpSecurityConfig.configure(http);
        super.configure(http);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        authenticationConfig.configure(auth);
    }
}
