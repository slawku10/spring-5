package tacos.security;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.ldap.LdapAuthenticationProviderConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@Qualifier("LdapAuthentication")
public class LdapAuthentication extends AuthenticationManagerBuilderConfig {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        LdapAuthenticationProviderConfigurer<AuthenticationManagerBuilder> authenticationManagerBuilderLdapAuthenticationProviderConfigurer = auth
                .ldapAuthentication()
                .userSearchBase("ou=people")
                .userSearchFilter("(uid={0})")
                .groupSearchFilter("ou=groups")
                .groupSearchFilter("member={}");

        authenticationManagerBuilderLdapAuthenticationProviderConfigurer
                .contextSource()
                .root("dc=tacocloud.dc=com")
                .ldif("classpath:users.ldif");

        authenticationManagerBuilderLdapAuthenticationProviderConfigurer
                .passwordCompare()
                .passwordEncoder(new BCryptPasswordEncoder())
                .passwordAttribute("passcode");
    }
}
