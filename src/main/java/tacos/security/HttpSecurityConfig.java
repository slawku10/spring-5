package tacos.security;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

abstract class HttpSecurityConfig {
    abstract void configure(HttpSecurity http) throws Exception;
}
